import * as React from "react";
import * as ReactDOM from "react-dom";
import { ipcRenderer } from "electron";

import App from "./app/reactApp";

ipcRenderer.on("login-success", (event, data) => {
  console.log("in ipcRenderer", data);
});

function render() {
  const rootNode = document.getElementById("root");
  ReactDOM.render(<App />, rootNode);
}

render();
