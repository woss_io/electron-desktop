/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { BrowserWindow } from "electron";
import authService from "../app/services/authService";
import createAppWindow from "../windows/appWindow";

let win: BrowserWindow = null;

function createAuthWindow() {
  destroyAuthWin();

  win = new BrowserWindow({
    width: 400,
    height: 680,
    webPreferences: {
      nodeIntegration: false,
      enableRemoteModule: false,
    },
  });

  win.loadURL(authService.getAuthenticationURL());
  // win.webContents.openDevTools();

  const {
    session: { webRequest },
  } = win.webContents;

  const filter = {
    urls: ["http://localhost:1234/callback*"],
  };

  webRequest.onBeforeRequest(filter, async ({ url }) => {
    await authService.loadTokens(url);
    createAppWindow();
    return destroyAuthWin();
  });

  win.on("closed", () => {
    win = null;
  });
  win.on("ready-to-show", () => {
    win = null;
  });

  // win.on("authenticated", () => {
  //   destroyAuthWin();
  // });
}

function destroyAuthWin() {
  if (!win) return;
  win.close();
  win = null;
}

function createLogoutWindow() {
  const logoutWindow = new BrowserWindow({
    show: false,
  });

  logoutWindow.loadURL(authService.getLogOutUrl());

  logoutWindow.on("ready-to-show", async () => {
    logoutWindow.close();
    await authService.logout();
  });
}

export default {
  createAuthWindow,
  createLogoutWindow,
};
