/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { BrowserWindow } from "electron";
declare const MAIN_WINDOW_WEBPACK_ENTRY: any;
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: any;

export const redirectUri = "http://localhost:1234/callback";

export default function createAuthWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1800,
    height: 1024,
    webPreferences: {
      preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
      nodeIntegration: true, // default value is false after Electron v5
      enableRemoteModule: true,
      // contextIsolation: true, // protect against prototype pollution
      // webSecurity: false,
    },
  });

  // and load the index.html of the app.
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

  // Open the DevTools.
  mainWindow.webContents.openDevTools();
  // mainWindow.webContents.send("login-success", tokens);
  // const {
  //   session: { webRequest },
  // } = mainWindow.webContents;

  // const filter = {
  //   urls: [
  //     `${redirectUri}*`,
  //     "http://localhost:1234/loggedOut",
  //     // "*://sensio.eu.auth0.com/*",
  //   ],
  // };

  // webRequest.onBeforeRequest(filter, async (params) => {
  //   const { url } = params;
  //   console.log("on before request", params, url);
  //   mainWindow.webContents.send("login-success", tokens);
  // });

  // webRequest.onBeforeSendHeaders(filter, (details, callback) => {
  //   // details.requestHeaders["Origin"] = null;
  //   // details.headers["Origin"] = null;
  //   callback({ requestHeaders: details.requestHeaders });
  // });
}
