import * as React from "react";
// https://www.electronforge.io/config/plugins/webpack#how-do-i-do-virtual-routing
import { MemoryRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "./pages/home";
import Profile from "./pages/profile";
import Store from "electron-store";

export const store = new Store();

const App: React.FC = () => {
  React.useEffect(() => {
    const tokens = store.get("tokens");
    console.log("tokens", tokens);
  });
  return (
    <Router keyLength={16}>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/profile">Profile</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
        renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/profile" component={Profile} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
