import React from "react";
import { remote } from "electron";
import loginWindow from "../../windows/loginWindow";
const LogoutButton: React.FC = () => {
  return (
    <button
      className="btn btn-primary btn-block"
      onClick={() => {
        loginWindow.createLogoutWindow();
        remote.getCurrentWindow().close();
      }}
    >
      Logout
    </button>
  );
};

export default LogoutButton;
