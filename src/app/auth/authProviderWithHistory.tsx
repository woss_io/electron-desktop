/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { useHistory } from "react-router-dom";
import { Auth0Provider } from "@auth0/auth0-react";
import envVariables from "../../env-variables.json";

export const redirectUri = "http://localhost:1234/callback";

const Auth0ProviderWithHistory: React.FC = ({ children }) => {
  const history = useHistory();

  const onRedirectCallback = (appState: { returnTo: any }) => {
    history.push(appState?.returnTo || window.location.pathname);
  };

  const { apiIdentifier, auth0Domain, clientId } = envVariables;
  return (
    <Auth0Provider
      domain={auth0Domain}
      clientId={clientId}
      redirectUri={redirectUri}
      onRedirectCallback={onRedirectCallback}
      audience={apiIdentifier}
    >
      {children}
    </Auth0Provider>
  );
};

export default Auth0ProviderWithHistory;
