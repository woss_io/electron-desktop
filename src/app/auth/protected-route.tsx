/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { Route } from "react-router-dom";
import { withAuthenticationRequired } from "@auth0/auth0-react";

const ProtectedRoute: React.FC<any> = (props) => {
  const { component, ...args } = props as any;

  return (
    <Route
      component={withAuthenticationRequired(component, {
        onRedirecting: () => <div>Loading...</div>,
      })}
      {...args}
    />
  );
};

export default ProtectedRoute;
