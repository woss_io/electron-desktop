/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import LoginButton from "../components/login-button";
import LogoutButton from "../components/logout-button";
import { store } from "../reactApp";

const Home: React.FC = () => {
  const tokens: any = store.get("tokens");
  const isAuthenticated = !!tokens.accessToken;

  return (
    <>
      <div>
        {isAuthenticated && (
          <div>
            <LogoutButton />
          </div>
        )}
      </div>
      <div>{!isAuthenticated && <LoginButton />}</div>
    </>
  );
};

export default Home;
