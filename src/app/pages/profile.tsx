/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { store } from "../reactApp";
// import { useAuth0 } from "@auth0/auth0-react";

const Profile: React.FC = () => {
  const tokens: any = store.get("tokens");
  if (!tokens || !tokens.profile) {
    return <div>No tokens found thus no identity!!!</div>;
  }
  const { picture, name, email } = tokens.profile;

  return (
    <div>
      <div className="row align-items-center profile-header">
        <div className="col-md-2 mb-3">
          <img
            src={picture}
            alt="Profile"
            className="rounded-circle img-fluid profile-picture mb-3 mb-md-0"
          />
        </div>
        <div className="col-md text-center text-md-left">
          <h2>{name}</h2>
          <p className="lead text-muted">{email}</p>
        </div>
      </div>
      <div className="row">
        <pre className="col-12 text-light bg-dark p-4">
          {JSON.stringify(tokens.profile, null, 2)}
        </pre>
      </div>
    </div>
  );
};

export default Profile;
