/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import jwtDecode from "jwt-decode";
import axios from "axios";
import url from "url";
import envVariables from "../../env-variables.json";
import Store from "electron-store";

const store = new Store();

const { apiIdentifier, auth0Domain, clientId } = envVariables;

const redirectUri = "http://localhost:1234/callback";

function getAccessToken() {
  const tokens: any = store.get("tokens");
  return tokens.accessToken;
}

function getProfile() {
  const tokens: any = store.get("tokens");
  return tokens.profile;
}

function getAuthenticationURL() {
  return (
    "https://" +
    auth0Domain +
    "/authorize?" +
    "audience=" +
    apiIdentifier +
    "&" +
    "scope=openid profile offline_access&" +
    "response_type=code&" +
    "client_id=" +
    clientId +
    "&" +
    "redirect_uri=" +
    redirectUri
  );
}

async function refreshTokens() {
  const tokens: any = store.get("tokens");

  if (tokens.refreshToken) {
    const refreshOptions: any = {
      method: "post",
      url: `https://${auth0Domain}/oauth/token`,
      headers: { "content-type": "application/json" },
      data: {
        grant_type: "refresh_token",
        client_id: clientId,
        refresh_token: tokens.refreshToken,
      },
    };

    try {
      const response = await axios(refreshOptions);
      console.log("refresh token ", response.data);
      const ret = {
        accessToken: response.data.access_token,
        profile: jwtDecode(response.data.id_token),
      };
      store.set("tokens", { ...tokens, ret });
    } catch (error) {
      await logout();

      throw error;
    }
  } else {
    throw new Error("No available refresh token.");
  }
}

async function loadTokens(callbackURL: string) {
  const urlParts = url.parse(callbackURL, true);
  const query = urlParts.query;

  const exchangeOptions = {
    grant_type: "authorization_code",
    client_id: clientId,
    code: query.code,
    redirect_uri: redirectUri,
  };

  const options: any = {
    method: "post",
    url: `https://${auth0Domain}/oauth/token`,
    headers: {
      "content-type": "application/json",
    },
    data: JSON.stringify(exchangeOptions),
  };

  try {
    const response = await axios(options);

    const ret = {
      refreshToken: response.data.refresh_token,
      accessToken: response.data.access_token,
      profile: jwtDecode(response.data.id_token),
    };

    store.set("tokens", ret);
    return ret;
  } catch (error) {
    await logout();

    throw error;
  }
}

async function logout() {
  console.log("remove the stuff from storage");
}

function getLogOutUrl() {
  return `https://${auth0Domain}/v2/logout`;
}

export default {
  getAccessToken,
  getAuthenticationURL,
  getLogOutUrl,
  getProfile,
  loadTokens,
  logout,
  refreshTokens,
};
