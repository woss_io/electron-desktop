# How to start

```sh
git clone https://gitlab.com/woss_io/electron-desktop.git
cd electron-desktop
yarn

# rename env-variables.json.template to env-variables.json
# add the corresponding IDs

yarn start
```

To remove the app data:

Windows `del C:\Users\USERNAME\AppData\Roaming\desktop-app`

Mac `no idea atm`

Linux `no idea atm`

## Two-windows approach == MASTER branch

This is the approach that is discussed here https://auth0.com/blog/securing-electron-applications-with-openid-connect-and-oauth-2/

If i am not logged in i get redirected to the Universal Login subdomain where I enter correct email addr and the password. I get redirected to the callback URL which is caught by the [LoginWindow](/src/windows/loginWindow.tsx). Login window will get the user data using the `await authService.loadTokens(url);` method which is located in [AuthService](./src/app/services/authService.ts). Then it will store the data using the [electron-store](https://github.com/sindresorhus/electron-store/). When the data is stored it will create [MainAppWindow](./src/windows/appWindow.ts) which renders the react app. The react app can use the store to retrieve the stored data then implement the refresh_token logic inside the app.

This approach is really cumbersome when compared to the auth0-react and single window solution (if possible).

If i am logged in and i close the window, then open it, there is a check for existence and validity / expiration of the refresh_token, if error is not thrown the main process will create [MainAppWindow](./src/windows/appWindow.ts) instead of LoginWindow.
